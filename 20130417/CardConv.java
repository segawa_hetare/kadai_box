import java.util.Scanner;

class CardConv{
    public static void main(String[] args){
	Scanner stdIn = new Scanner(System.in);
	int no; // 変換する整数
	int n; // 基数
	int dno; // 変換後の桁数
	char[] cno = new char[32];
	
	do{
	    System.out.print("変換する非負の整数  : ");
	    no = stdIn.nextInt();
	}while(no < 0);
	
	do{
	    System.out.print("何進数に変換しますか (2-36) : ");
	    n = stdIn.nextInt();
	}while(n < 2 || n  > 36);

	dno = cardConv(no, n, cno);
	reverse(cno, dno);
	
	System.out.print(no + " は " + n + "進数では ");
	for(int i = 0; i < dno; i++){
	    System.out.print(cno[i]);
	}
	System.out.println(" です。");
    }

    static int cardConv(int x, int r, char[] d){
	int digits = 0;
	String dchar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	do{
	    d[digits++] = dchar.charAt(x % r);
	    x /= r;
	}while(x != 0);

	//reverse(d);
	
	return digits;
    }

    static void reverse(char[] array, int d){
	for(int i = 0; i < (int)(d / 2); i++){
	    swap(array, i, d-i-1);
	}
    }

    static void swap(char[] array, int idx1, int idx2){
	char tmp;
	tmp = array[idx1];
	array[idx1] = array[idx2];
	array[idx2] = tmp;
    }
}