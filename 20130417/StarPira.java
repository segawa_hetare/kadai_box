import java.util.Scanner;

class StarPira{
    public static void main(String[] args){
	Scanner stdIn = new Scanner(System.in);
	int n;

	do{
	    System.out.print("段差の入力 : ");
	    n = stdIn.nextInt();
	}while(n < 0);

	spira(n);
    }

    static void spira(int n){
	int x = 2*n-1;
	char[] star = new char[x];
	star[x/2] = '*';
	for(int i = 0; i < x; i++){
	    if(star[i] != '*')
		star[i] = ' ';
	}
	System.out.println(star);

	for(int i = 0;  i < x/2; i++){
	    star[x/2+i+1] = '*';
	    star[x/2-i-1] = '*';
	    System.out.println(star);
	}

    }
}