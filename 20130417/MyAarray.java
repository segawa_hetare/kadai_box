import java.util.Random;
import java.util.Scanner;

class MyAarray{
    public static void main (String[] args){
	Scanner stdIn = new Scanner(System.in);
	int n;

	do{
	    System.out.print("要素数を入力してください : ");
	    n = stdIn.nextInt();
	}while(n <= 0);
	System.out.println("要素数 = " + n);

	int[] data = new int[n];
	generateRandomValues(data);
	show(data);
    } 

    static void generateRandomValues(int[] array){
	Random rnd = new Random();
	for(int i = 0; i < array.length; i++){
	    array[i] = rnd.nextInt(90);
	}
    }

    static void show(int[] array){
	for(int i = 0; i < array.length; i++){
	    System.out.println("data[" + i + "] = " + array[i]);
	}
	System.out.println();
	reverse(array);
	System.out.println("逆順");
	System.out.println("要素数 = " + array.length);
	for(int i = 0; i< array.length; i++){
	    System.out.println("data[" + i + "] = " + array[i]);
	}
    }

    static void swap(int[] array, int idx1, int idx2){
	int tmp;
	tmp = array[idx1];
	array[idx1] = array[idx2];
	array[idx2] = tmp;
    }

    static void reverse(int[] array){
	for(int i = 0; i < (int)(array.length / 2); i++){
	    swap(array, i, array.length-i-1);
	}
    }
}
