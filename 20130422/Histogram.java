import java.util.Random;
import java.util.Scanner;

class Histogram{
    public static void main (String[] args){
	Scanner stdIn = new Scanner(System.in);
	int n;
	int max;

	do{
	    System.out.print("乱数の個数 n の入力：");
	    n = stdIn.nextInt();
	    System.out.print("乱数の最大値 max の入力:");
	    max = stdIn.nextInt();

	}while(n <= 0 || max <= 0);
	System.out.println("要素数 = " + n);

	int[] data = new int[n];
	generateRandomValues(data, max);
	show(data);
    } 

    static void generateRandomValues(int[] array, int maxValue){
	Random rnd = new Random();
	for(int i = 0; i < array.length; i++){
	    array[i] = rnd.nextInt(maxValue);
	}
    }

    static void show(int[] array){
	for(int i = 0; i < array.length; i++){
	    System.out.print(i + ": " + array[i] + " ");
	    for(int j = 0; j < array[i]; j++){
		System.out.print("o");
	    }
	    System.out.println();
	}
	
    }
}
