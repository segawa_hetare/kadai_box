class MyMatrix {
    public static void main(String[] args){
	int[][] mA = {{1, 2},
		      {3, 4},
		      {5, 6}};
	int[][] mB = {{6, 3},
		      {4, 5},
		      {1, 2}};
	int[][] mC = new int[3][2];
	int[][] mD = new int[3][2];

	add(mA, mB, mC);
	sub(mA, mB, mD);

	System.out.println("mA");
	showMatrix(mA);
	System.out.println("mB");
	showMatrix(mB);
	System.out.println("mC");
	showMatrix(mC);
	System.out.println("mD");
	showMatrix(mD);
    }

    static void showMatrix(int[][] array){
	for(int i = 0; i < 3; i++){
	    for(int j = 0; j < 2; j++){
		System.out.printf("%4d", array[i][j]);
	    }
	    System.out.println();
	}
    }

    static void add(int[][] a, int[][] b, int[][] c){
	for(int i = 0; i < 3; i++){
	    for(int j = 0; j < 2; j++){
		c[i][j] = a[i][j] + b[i][j];
	    }
	}
    }
    
    static void sub(int[][] a, int[][] b, int[][] c){
	for(int i = 0; i < 3; i++){
	    for(int j = 0; j < 2; j++){
		c[i][j] = a[i][j] -  b[i][j];
	    }
	}
    }
}