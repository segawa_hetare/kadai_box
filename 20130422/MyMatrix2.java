class MyMatrix {
    public static void main(String[] args){
	int[][] mA = {{1, 2},
		      {0, -1},
		      {5, 1}};
	int[][] mB = {{1, -2, 4, 0},
		      {0, -1, 2, 3}};
	int[][] mC = new int[3][4];

	multiply(mA, mB, mC);

	System.out.println("mA");
	showMatrix(mA, 2, 3);
	System.out.println("mB");
	showMatrix(mB, 4, 2);
	System.out.println("mC");
	showMatrix(mC, 4, 3);
    }

    static void showMatrix(int[][] array, int width, int height){
	for(int i = 0; i < height; i++){
	    for(int j = 0; j < width; j++){
		System.out.printf("%4d", array[i][j]);
	    }
	    System.out.println();
	}
    }
    
    static void multiply(int[][] a, int[][] b, int[][] c){
	for(int k = 0; k < 4; k++){
	    for(int i = 0; i < 3; i++){
		for(int j = 0; j < 2; j++){
		    c[i][k] += a[i][j]*b[j][k];
		}
	    }
	}
    }
}