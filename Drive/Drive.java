import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JApplet;
import javax.swing.Timer;


public class Drive extends JApplet implements ActionListener, KeyListener{
	// 1秒前の速度 V(n-1)
	public double Vnm1_x;
	public double Vnm1_y;
	// 現在の速度 V(n)
	public double Vn_x;
	public double Vn_y;
	// スカラー摩擦因子
	public double F = 0;
	// 加速度ベクトル
	public double A = 0;
	public double maxA = 500;
	public double initA = 10;

	// 回転角度
	public double rot = 0;
	// 1秒前の回転速度
	public double rotm1;
	// 回転加速度
	public double drot = 0;
	public double maxDrout = 30;
	public double minDrout = -30;
	public double initDrout1 = 10;
	public double initDrout2 = -10;

	public int playerX = 200;
	public int playerY = 200;

	public Timer timer;
	public Timer fpsTimer;
	public int width = 0;
	public int height = 0;
	Image backImage = null;
	Image workImage = null;

	boolean isRight = false,
			isLeft = false,
			isAccel = false,
			isBrake = false;


	public void init(){
		this.rot = 0;
		this.A = 0;

		Container pane = getContentPane();
		pane.setBackground(Color.GREEN);

		this.addKeyListener(this);
		this.setFocusable(true);

		timer = new Timer(1000, this);
		fpsTimer = new Timer(2000/60, this);
	}

	public void start(){
		this.Vnm1_x = 0.0;
		this.Vnm1_y = 0.0;
		this.rotm1 = 0.0;
		//timer.start();
		fpsTimer.start();
	}

	public void stop(){
		//timer.stop();
		fpsTimer.stop();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();

		switch(keyCode){
		case KeyEvent.VK_UP:
			this.A = this.initA;
			this.isAccel = true;
			break;
		case KeyEvent.VK_RIGHT:
			this.drot = this.initDrout2;
			this.isRight = true;
			break;
		case KeyEvent.VK_LEFT:
			this.drot = this.initDrout1;
			this.isLeft = true;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode();

		switch(keyCode){
		case KeyEvent.VK_UP:
			this.isAccel = false;
			repaint();
			break;
		case KeyEvent.VK_RIGHT:
			this.isRight = false;
			break;
		case KeyEvent.VK_LEFT:
			this.isLeft = false;
			break;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if(timer == src){
			updateState();
		}else if(fpsTimer == src){
			updateState();
			repaint();
		}
	}

	public void updateState(){
		// 計算部分
		// 操縦部分
		if(this.isRight == true){
			if(this.drot <= this.minDrout){
				this.drot = this.minDrout;
			}else{
				this.drot -= 0.1;
			}
		}else{
			if(this.drot >= 0){
				this.drot = 0;
			}else{
				this.drot += 0.2;
			}
		}
		if(this.isLeft == true){
			if(this.drot >= this.maxDrout){
				this.drot = this.maxDrout;
			}else{
				this.drot += 0.1;
			}
		}else{
			if(this.drot <= 0){
				this.drot = 0;
			}else{
				this.drot -= 0.2;
			}
		}

		// アクセル計算
		if(this.isAccel == true){
			if(this.A > this.maxA){
				this.A = this.maxA;
			}else{
				this.A++;
			}
		}else{
			if(this.A <= 0){
				this.A = 0;
			}else{
				this.A -= 2;
			}
		}

		this.rot = this.rotm1 - this.drot;
		if(this.rot >= 360){
			this.rot -= 360;
		}

		// ベクトル計算
		this.Vn_x = this.Vnm1_x * F;
		this.Vn_y = this.Vnm1_y * F;

		this.Vn_x += A*Math.cos(rot);
		this.Vn_y += A*Math.sin(rot);

		this.playerX += (int) (this.Vn_x);
		this.playerY += (int) (this.Vn_y);

		if(this.playerX < 0){
			this.playerX = 0;
		}else if(this.playerX > getWidth()){
			this.playerX = getWidth();
		}if(this.playerY < 0){
			this.playerY = 0;
		}else if(this.playerY > getHeight()){
			this.playerY = getHeight();
		}

		// n-1計算
		this.Vnm1_x = this.Vn_x;
		this.Vnm1_y = this.Vn_y;
		this.rotm1 = this.rot;

		repaint();

	}

	public void paint(Graphics g){
		if(backImage == null || width != getWidth() || height != getHeight()){
			width = getWidth();
			height = getHeight();
			createBackImage();
		}

		createWorkImage();
		g.drawImage(workImage, 0, 0, this);
	}

	public void createBackImage(){
		backImage = createImage(width, height);
		Graphics g = backImage.getGraphics();

		g.clearRect(0, 0, width, height);
	}

	public void createWorkImage(){
		workImage = createImage(width, height);
		Graphics g = workImage.getGraphics();

		g.drawImage(backImage, 0, 0, this);

		// ここに描画
		g.setColor(Color.BLUE);
		Graphics2D g2 = (Graphics2D)g;
		double rot = this.rot;
		g2.rotate(rot, playerX, playerY);

		int playerX = this.playerX - 10;
		int playerY = this.playerY - 20;
		System.out.println(this.rot + " "+this.playerX+" "+this.playerY+" "+this.drot);

		g2.fillRect(playerX, playerY, 20, 40);

	}

	@Override
	public void keyTyped(KeyEvent e) {}
}
