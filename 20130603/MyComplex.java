class MyComplex{
    double real, imag;

    MyComplex(double x, double y){
	real = x;
	imag = y;
    }

    MyComplex(double x){
	real = x;
	imag = 0;
    }

    public String toString(){
	return (real + " + " + imag + "i");
    }

    double real(){
	double real = this.real;
	return real;
    }

    double imag(){
	double imag = this.imag;
	return imag;
    }

    static MyComplex add(MyComplex a, MyComplex b){
	MyComplex ans = new MyComplex(a.real, a.imag);
	ans.real += b.real;
	ans.imag += b.imag;

	return ans;
    }

    static MyComplex sub(MyComplex a, MyComplex b){
	MyComplex ans = new MyComplex(a.real, a.imag);
	ans.real -= b.real;
	ans.imag -= b.imag;

	return ans;
    }

    static MyComplex mul(MyComplex a, MyComplex b){
	MyComplex ans = new MyComplex(a.real, a.imag);
	ans.real = a.real*b.real - a.imag*b.imag;
	ans.imag = a.real*b.imag+a.imag*b.real;

	return ans;
    }

    static MyComplex div(MyComplex a, MyComplex b){
	MyComplex ans = new MyComplex(a.real, a.imag);
	ans.real = (a.real*b.real+a.imag*b.imag)/(b.real*b.real+b.imag*b.imag);
	ans.imag = (a.imag*b.real-a.real*b.imag)/(b.real*b.real+b.imag*b.imag);
	return ans;
    }

    MyComplex add (MyComplex a){
	MyComplex ans = new MyComplex(this.real, this.imag);
	ans.real += a.real;
	ans.imag += a.imag;
	return ans;
    }

    MyComplex sub(MyComplex a){
	MyComplex ans = new MyComplex(this.real, this.imag);
	ans.real -= a.real;
	ans.imag -= a.imag;
	return ans;
    }

    MyComplex mul(MyComplex a){
	MyComplex ans = new MyComplex(this.real, this.imag);
	ans.real = this.real*a.real - this.imag*a.imag;
	ans.imag = this.real*a.imag + this.imag*a.real;
	return ans;
    }

    MyComplex div(MyComplex a){
	MyComplex ans = new MyComplex(this.real, this.imag);
	ans.real = (this.real*a.real+this.imag*a.imag)/(a.real*a.real+a.imag*a.imag);
	ans.imag = (this.imag*a.real-this.real*a.imag)/(this.real*a.real+a.imag*a.imag);
	return ans;
    }

    MyComplex con(){
	this.imag *= -1;
	return new MyComplex(this.real, this.imag);
    }
}